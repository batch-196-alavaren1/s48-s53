import {useState} from 'react';
import {Card, Button} from 'react-bootstrap';
import {Link} from 'react-router-dom';


export default function CourseCard({courseProp}){
	// check to see if data was successfully passed
	// console.log(props);
		// result: php-laravel (coursesData[0])
	// every component receive informmation in a form of object
	// console.log(typeof props);

	// object destructuring
	const {name, description, price, _id} = courseProp;


	// react hook-useState -> store its state
	// Syntax:
		// const [getter, setter] = useState(initialGetterValue);
	// const [count, setCount] = useState(0);
	// const [counts, seatCount] = useState(10);
	// console.log(useState(0));


	// function enroll(){
	// 	if(counts > 0){
	// 		setCount(count + 1)
	// 		console.log(`Enrollees: ${count}`);
	// 		seatCount(counts - 1)
	// 		console.log(`Seats: ${counts}`);
	// 	} else {
	// 		alert("No more seats available, check back later.")
	// 	}
	// };

	return (
		<Card>
		      <Card.Body>
		        <Card.Title>{name}</Card.Title>
		        <Card.Subtitle>Course Description</Card.Subtitle>
		        <Card.Text>
		          {description}
		        </Card.Text>
		        <Card.Subtitle>Course Price</Card.Subtitle>
		        <Card.Text>
		          {price}
		        </Card.Text>
		        <Link className="btn btn-primary" to={`/courseView/${_id}`}>View Details</Link>
		      </Card.Body>
		    </Card>
	)
};